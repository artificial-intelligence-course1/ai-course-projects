#!/usr/bin/env python
# coding: utf-8

# <br>
# <b>
#     <font size="10" face="verdana">AI Fall 99 Project 1</font>
# </b>
# <hr>

# ## By Kimia Khabiri

# Introduction
# 
# The goal of this project is to recongnize sequence of gates that cause validity of truth table in the input using generic algorithms.

# In[3]:


import random

gates = {
    '0': "AND",
    '1': "OR",
    '2': "XOR",
    '3': "NAND",
    '4': "NOR",
    '5': "XNOR"
}

def xor(a,b):
    if(a == False and b == False): return False
    if(a == True and b == True): return False
    if(a == False and b == True): return True
    if(a == True and b == False): return True
    return

class GA():
    def __init__(self,inputs, output):
        self.inputs = inputs
        self.output = output
        self.chromosomSize = len(inputs[0]) - 1
        self.populationSize = 100
        self.population = self.makeFirstPopulation()
        self.crossoverProbability = 0.65
        self.carryPercentage = 0.2
        self.mutatationRate = 0.4
        self.matingSize = int(self.populationSize * (1-self.carryPercentage))
        self.generation = 1;
        self.maximumFitness = 0;
        self.maximumFitnessRepeat = 0;
        
    def findGates(self):
        while (True):
            if(self.maximumFitnessRepeat > 25):
                self.generation = 1
                self.population = self.makeFirstPopulation()
            random.shuffle(self.population)
            
            fitnesses = []
            for i in range(self.populationSize):
                fitness = self.calcFitness(self.population[i])
                if (fitness == -1):
                    return self.population[i]
                else:
                    fitnesses.append([fitness, i])
            fitnesses.sort(key=lambda x:x[0])
            if(fitnesses[-1][0] == self.maximumFitness):
                self.maximumFitnessRepeat += 1;
            else:
                self.maximumFitnessRepeat = 1;
                self.maximumFitness = fitnesses[-1][0]
#             print("Fitnesses: ")
#             for x in fitnesses:
#                 print(self.population[x[1]]," -> ",x[0])
#             print('\n\n\n')
            rankedChromosoms = [x[1] for x in fitnesses]
            matingPool = self.makeMatingPool(rankedChromosoms)
#             print("matingPool: ")
#             for x in matingPool:
#                 print(x)
#             print('\n\n\n')
            crossoverPool = self.makeCrossoverPool(matingPool)
#             print("crossover: ")
#             for x in matingPool:
#                 print(x)
#             print('\n\n\n')
            
            carry = []
            for i in range(1, int(self.populationSize*self.carryPercentage) + 1):
                carry.append(self.population[rankedChromosoms[-i]])
            
            self.population.clear()
            for i in range(self.matingSize):
                self.population.append(self.mutate(crossoverPool[i]))
                
            self.population.extend(carry)
            self.generation += 1;

    
    def makeMatingPool(self, rankedChromosoms):
        #fitnessItems : [chromosomeFitness, chromosomeIndex]
        matingPool = []
        probabilityPool = []
        ranks = range(0, self.populationSize)
        for rank in ranks:
            for i in range(rank+1):
                probabilityPool.append(rankedChromosoms[rank])
        random.shuffle(probabilityPool)
        for i in range(self.matingSize):
            randomChoice = random.randint(0, len(probabilityPool) - 1)
            matingPool.append(self.population[probabilityPool[randomChoice]])
        return matingPool
    
    def makeCrossoverPool(self, matingPool):
        crossoverPool = []
        for i in range(int(len(matingPool) / 2)):
            if random.random() > self.crossoverProbability:
                crossoverPool.append(matingPool[i*2])
                crossoverPool.append(matingPool[i*2 + 1])
            else:
                parent1 = matingPool[i*2]
                parent2 = matingPool[i*2 + 1]
                child1 = ""
                child2 = ""
                position1 = random.randint(0, int(self.chromosomSize/2))
                position2 = random.randint(0, int(self.chromosomSize/2))
                if(position2 == position1):
                    position2 = random.randint(0, int(self.chromosomSize/2))
                if(position1 > position2):
                    temp = position1
                    position1 = position2
                    position2 = temp
                child1 = parent1[:position1] + parent2[position1:position2] + parent1[position2:]
                child2 = parent2[:position1] + parent1[position1:position2] + parent2[position2:]
                crossoverPool.append(child1)
                crossoverPool.append(child2)
        return crossoverPool
    
    def mutate(self, chromosome):
        if random.random() <= self.mutatationRate:
            selectedGate = random.randint(0, int(self.chromosomSize)-1)
            replacement = random.randint(0,5)
            if(replacement == chromosome[selectedGate]):
                replacement = random.randint(0,5)
            chromosome = chromosome[:selectedGate] + str(replacement) + chromosome[selectedGate+1:]
        return chromosome
        
    def makeRandomChromosome(self):
        chromosome = ''
        for i in range(self.chromosomSize):
            randomGate = random.randint(0,5)
            chromosome += str(randomGate)
        return chromosome
    
    def makeFirstPopulation(self):
        firstPopulation = []
        for i in range(self.populationSize):
            newChromosome = self.makeRandomChromosome()
            firstPopulation.append(newChromosome)
        return firstPopulation
    
    def calcFitness(self, chromosome):
        fitness = 0
#         print("Chromose is: ",chromosome)
        for i in range(len(self.inputs)):
            result = ''
            input = self.inputs[i]
#             print("Applying chromosome on input: ", input)
            for j in range(self.chromosomSize):
                if(result == ''):
                    result = self.applyGate(input[j], input[j+1], chromosome[j])
                else:
                    result = self.applyGate(result, input[j+1], chromosome[j])
#             print("Output must be: ",output[i]," | Output is: ", result)
            if(result == self.output[i]):
                fitness += 1
        if(fitness == len(self.output)):
#             print("Calculated fitness: ",fitness, " length of output: ",len(self.output))
            return -1
        return fitness
    
    def applyGate(self,input1, input2, gate):
        #AND
        if(gate == '0'):
            return input1 and input2
        #OR
        elif(gate == '1'):
            return input1 or input2
        #XOR
        elif(gate == '2'):
            return xor(input1, input2)
        #NAND
        elif(gate == '3'):
            return not (input1 and input2)
        #NOR
        elif(gate == '4'):
            return not (input1 or input2)
        #NXOR
        elif(gate == '5'):
            return not xor(input1, input2)    
        return "Invalid Gate"                    
        
            


# In[4]:


import re
import time

f = open("truth_table.csv").read()
rows = re.split('\n',f)
rows.pop(-1)
booleanTable = []
for row in rows[1:]:
    values = re.split(',',row)
    booleanRow = []
    for value in values:
        if(value == "FALSE"):
            booleanRow.append(False)
        elif(value == "TRUE"):
            booleanRow.append(True)
    booleanTable.append(booleanRow)
# print(booleanTable)
inputs = []
output = []
for row in booleanTable:
    inputs.append(row[:-1])
    output.append(row[-1])
geneticAlgorithm = GA(inputs, output)
tic = time.time()
gates = geneticAlgorithm.findGates()
toc = time.time()
print("time is ", (toc - tic))
print("Population size: ",geneticAlgorithm.populationSize)
print("Gates: ", gates)
print("Generation: ", geneticAlgorithm.generation)


# In[ ]:




